## Resource

Icons: https://react-icons.github.io/react-icons

Color Palettes: https://coolors.co/ , https://yeun.github.io/open-color/

Contrast check: https://coolors.co/contrast-checker/112a46-acc8e5

Html Entity: https://tools.w3cub.com/html-entities

Favicon: https://realfavicongenerator.net/

Tinst and shades: https://maketintsandshades.com/

Placeholder: https://placeholderimage.dev/

Features Check: https://caniuse.com/

## Tech Stack

**Tech Used:** Next.js 13, TailwindCSS

**Packages:** SWR hook

## Run Locally

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run dev

```
