import React from 'react'

export default function Details({ text, description }) {
  return (
    <div className="moveBottomAnimation text-[#eee]  px-10  ">
      <h1 className="mt-6 text-xl font-bold text-center  md:text-3xl">
        {text}
      </h1>
      <p className="mt-5 text-xs italic text-center  md:text-sm">
        {description}
      </p>
    </div>
  )
}
