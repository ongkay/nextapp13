export default function Head() {
  return (
    <>
      <title>Next.js v13 example</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <meta
        name="description"
        content="ini adalah projek ecommerce percobaan"
      />
      <meta
        name="keywords"
        content="buat ecommerce, jual beli, mantul ecommerce, tokoku mantap"
      />
      <meta name="author" content="saya sendiri" />
      <link rel="icon" href="/favicon.ico" />
    </>
  )
}
