import Details from '../components/Details'
import MovieList from '../components/MovieList'
import fetchData from '../utils/fetchData'

export default function HomeSsr() {
  const data = fetchData('/movie/popular', 'ssr')

  return (
    <section className="pt-20 ">
      <Details
        description="Halaman ini menggunakan SSR fetching data ke TMDB movie menampilkan movie yang sedang tranding saat ini."
        text="GetServerSideProps"
      />
      <MovieList results={data?.results} forSsr />
    </section>
  )
}
