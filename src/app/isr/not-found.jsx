export default function NotFound() {
  return (
    <div className=" flex justify-center  gap-5 items-center h-[100vh]">
      <p className="text-2xl font-bold ">Halaman tidak di temukan</p>
    </div>
  )
}
