import React from 'react'
import Details from '../../components/Details'
import fetchData from '../../utils/fetchData'
import MovieList from '../../components/MovieList'

export default function HomeIsr() {
  const data = fetchData('/movie/popular', 'isr', 5)

  return (
    <section className="pt-20 ">
      <Details
        text="GetStaticProps-Revalidate"
        description="Halaman ini menggunakan ISR Revalide 5detik fetching data ke TMDB movie menampilkan movie yang sedang tranding saat ini."
      />
      <MovieList results={data.results} forIsr />
    </section>
  )
}
