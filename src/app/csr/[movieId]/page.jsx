/* eslint-disable react-hooks/rules-of-hooks */
'use client'
import useSWR from 'swr'
import MovieDetail from '../../../components/MovieDetail'
import fetchData from '../../../utils/fetchData'
import CustomLoading from '../../../components/CustomLoading'
import ErrorCustom from '../../../components/ErrorCustom'

export default function DetailMovieCsr({ params }) {
  const { movieId } = params
  const { data, error } = useSWR(`/movie/${movieId}`, fetchData)

  if (error) return <ErrorCustom />
  if (!data && !error) return <CustomLoading />

  return (
    <section className="flex justify-center items-center h-screen px-5 bg-gradient-to-t from-[#121212] to-[#ccc]">
      <MovieDetail data={data} />
    </section>
  )
}
