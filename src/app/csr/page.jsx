/* eslint-disable react-hooks/rules-of-hooks */
'use client'
import React from 'react'
import Details from '../../components/Details'
import fetchData from '../../utils/fetchData'
import MovieList from '../../components/MovieList'
import useSWR from 'swr'
import CustomLoading from '../../components/CustomLoading'
import ErrorCustom from '../../components/ErrorCustom'

export default function HomeCsr() {
  const { data, error } = useSWR('/movie/popular', fetchData)

  if (error) return <ErrorCustom />
  if (!data && !error) return <CustomLoading />
  return (
    <section className="pt-20 ">
      <Details
        text="Client Site Rendering"
        description="Halaman ini menggunakan CSR dan useSWR, fetching data ke TMDB movie menampilkan movie yang sedang tranding saat ini."
      />
      <MovieList results={data.results} forCsr />
    </section>
  )
}
