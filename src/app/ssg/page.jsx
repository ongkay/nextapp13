import React from 'react'
import Details from '../../components/Details'
import fetchData from '../../utils/fetchData'
import MovieList from '../../components/MovieList'

export default function HomeSsg() {
  const data = fetchData('/movie/popular', 'ssg')

  return (
    <section className="pt-20 ">
      <Details
        text="GetStaticProps"
        description="Halaman ini menggunakan SSG fetching data ke TMDB movie menampilkan movie yang sedang tranding saat ini."
      />
      <MovieList results={data.results} forSsg />
    </section>
  )
}
