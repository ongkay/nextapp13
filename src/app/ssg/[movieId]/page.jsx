import MovieDetail from '../../../components/MovieDetail'
import fetchData from '../../../utils/fetchData'

export default function DetailMovieSsg({ params }) {
  const { movieId } = params
  const data = fetchData(`/movie/${movieId}`, 'ssg')

  return (
    <section className="flex justify-center items-center h-screen px-5 bg-gradient-to-t from-[#121212] to-[#ccc]">
      <MovieDetail data={data} />
    </section>
  )
}

export async function generateStaticParams() {
  const data = await fetchData(`/movie/popular`)

  return data?.results?.map((item) => ({
    movieId: item.id.toString(),
  }))
}
