import MovieDetail from '../../components/MovieDetail'
import fetchData from '../../utils/fetchData'

export default function DetailMovieSsr({ params }) {
  const { movieId } = params
  const data = fetchData(`/movie/${movieId}`, 'ssr')

  return (
    <section className="flex justify-center items-center h-screen px-5 bg-gradient-to-t from-[#121212] to-[#ccc]">
      <MovieDetail data={data} />
    </section>
  )
}
