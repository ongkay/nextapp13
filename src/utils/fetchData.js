import apiTmdb from '../configs/apiTmdb'
import { use } from 'react'

export const getData = async (url, option) => {
  try {
    const endPoint = apiTmdb.baseUrl + url + apiTmdb.apiKey

    // let response
    // if (option) {
    //   response = await fetch(endPoint, option)
    // } else {
    //   response = await fetch(endPoint)
    // }

    const response = option
      ? await fetch(endPoint, option)
      : await fetch(endPoint)

    return response.json()
  } catch (err) {
    console.log('fetching Erorrrrrrr gais')
    console.log(url)
  }
}

const fetchData = (url, type, revalidate = 10) => {
  switch (type) {
    case 'ssg':
      return use(getData(url, { cache: 'force-cache' }))
    case 'ssr':
      return use(getData(url, { cache: 'no-store' }))
    case 'isr':
      return use(
        getData(url, {
          next: { revalidate },
        }),
      )
    default:
      return getData(url)
  }

  // if (type == 'ssg') {
  //   return use(getData(url, { cache: 'force-cache' }))
  // }

  // if (type == 'ssr') {
  //   return use(getData(url, { cache: 'no-store' }))
  // }

  // if (type == 'isr') {
  //   return use(
  //     getData(url, {
  //       next: { revalidate },
  //     }),
  //   )
  // }
}

export default fetchData
